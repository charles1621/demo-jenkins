package com.example.demo;

import org.junit.*;
import org.junit.runner.RunWith;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.logging.Logger;

import static org.junit.Assert.fail;

@RunWith(value = SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {

	Logger logger = Logger.getLogger(DemoApplicationTests.class.getName());
	static org.slf4j.Logger  log = LoggerFactory.getLogger(DemoApplicationTests.class);

	@Autowired
	private HelloService helloService;

	@BeforeClass
	public static void beforeClass() {
		log.info("beforeClass");
	}

	@AfterClass
	public static void afterClass() {
		log.info("afterClass");
	}

	@Before
	public void before() {
		log.info("before");
	}

	@After
	public void after() {
		log.info("after");
	}

	@Test()
	public void test1() {
		log.info("Se esta ejecutando el test 1");
	}

	@Test
	public void test2() {
		log.error("Se esta ejecutando el test 2");
		fail("Error intencional");
	}

	@Test
	public void testHello() {
		log.error(helloService.hola());
	}

}
